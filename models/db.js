const Datastore = require('nedb')

const createStore = (name) => {
  return new Datastore({ 
    filename: __dirname + `/data/${name}.db`,
    autoload: true
  })
}

module.exports = {
  users: createStore('users'),
  accounts: createStore('accounts'),
  transactions: createStore('transactions'),
}