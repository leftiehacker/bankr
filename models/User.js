class User {
  constructor (Username, Password) {
    if (!(Username || Password)) {
      throw "UserError: Invalid Username or Password."
    }

    this.Username = Username
    this.Password = Password
    this.IsLoggedIn = false
  }
}

module.exports = User