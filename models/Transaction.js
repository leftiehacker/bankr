class Transaction {
  constructor (AccountID, Type, Amount) {
    if (!(AccountID || Type || Amount)) {
      throw "TransactionError: Invalid AccountID, Type, or Amount"
    }

    this.AccountID = AccountID
    this.Type = Type
    this.Amount = Amount
    this.CreatedAt = new Date()
  }
}

module.exports = Transaction