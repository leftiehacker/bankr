class Account {
  constructor (UserID) {
    if (!UserID) {
      throw "AccountError: Invalid UserID."
    }

    this.UserID = UserID
    this.Balance = 0
  }
}

module.exports = Account