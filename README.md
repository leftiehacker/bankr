# What is Bankr?
Bankr is a command line application for tracking the deposits, withdrawals, and transaction history of your virtual Bankr account(s). 

## Install

```
npm install -g bankr
```

## Usage

### Create an account

```
bankr register <username>
```

*Note:* You will be prompted to create a password for your account.

### Log into your account

```
bankr login <username>
```

*Note:* You will be prompted to enter the password that you created during the registration process.

### Make a deposit

```
bankr deposit <username> <amount>
```

### Make a withdrawal

```
bankr withdraw <username> <amount>
```

### View your account balance

```
bankr balance <username>
```

### View your transaction history

```
bankr history <username>
```

### Log out of your account

```
bankr logout <username>
```