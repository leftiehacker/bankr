#!/usr/bin/env node
'use strict';

const program = require('commander')
const { version } = require('./package')
const {
  register,
  login,
  deposit,
  withdraw,
  balance,
  history,
  logout,
} = require('./controllers')

// version
program.version(version, '-v, --version')

// register
program
  .command('register <username>')
  .description('Create a new account')
  .action(register)

// login
program
  .command('login <username>')
  .description('Log into user account')
  .action(login)

// deposit
program
  .command('deposit <username> <amount>')
  .description('Deposit amount to user\'s account balance')
  .action(deposit)

// withdraw
program
  .command('withdraw <username> <amount>')
  .description('Withdraw amount from user\'s account balance')
  .action(withdraw)

// balance
program
  .command('balance <username>')
  .description('Check user\'s account balance')
  .action(balance)

// history
program
  .command('history <username>')
  .description('Get transaction history of user\'s account')
  .action(history)

// logout
program
  .command('logout <username>')
  .description('Log out of user\'s account')
  .action(logout)

program.parse(process.argv);