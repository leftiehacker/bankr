const co = require('co')
const prompt = require('co-prompt')

const { users } = require('../models/db')
const errorHandler = require('./util/errorHandler')

function login (Username) {
  users.findOne({ Username }, async (err, user) => {
    errorHandler(err, () => {
      if (!user) throw 'LoginError: Username not found.'
    })

    const Password = await co(function* () {
      const pwd = yield prompt.password('Password: ')
      return pwd
    })
  
    errorHandler(() => {
      if (Password !== user.Password) {
        throw 'LoginError: Incorrect password.'
      }
    })
      
    users.update(
      { _id: user._id },
      { $set: { IsLoggedIn: true } },
      (err, r) => {
        errorHandler(err)
        console.log('\x1b[32m', '\nLogin successful!')
      }
    )
  })
}

module.exports = login