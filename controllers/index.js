module.exports = {
  register: require('./register'),
  login: require('./login'),
  deposit: require('./deposit'),
  withdraw: require('./withdraw'),
  balance: require('./balance'),
  history: require('./history'),
  logout: require('./logout'),
}