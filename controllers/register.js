const co = require('co')
const prompt = require('co-prompt')

const { users, accounts } = require('../models/db')
const User = require('../models/User')
const Account = require('../models/Account')
const errorHandler = require('./util/errorHandler')

function register (Username) {
  const setPassword = async (err, user) => {
    errorHandler(err, () => {
      if (user) throw 'RegistrationError: Username already exists.'
    })

    const Password = await co(function* () {
      const pwd = yield prompt.password('Password: ')
      const confirm_pwd = yield prompt.password('Confirm password: ')
      
      errorHandler(() => {
        if (confirm_pwd != pwd) {
          throw 'RegistrationError: Passwords do not match.'
        }
      })
      return pwd
    })

    createUser(Password)
  }

  const createUser = (Password) => {
    const user = new User(Username, Password)
    users.insert(user, createAccount)
  }

  const createAccount = (err, user) => {
    errorHandler(err)

    const userId = user._id
    const account = new Account(userId)

    accounts.insert(account, (err, doc) => { 
      errorHandler(err)
      console.log('\x1b[34m', `\nWelcome to Bankr, ${Username}!`)
    })
  }

  users.findOne({ Username }, setPassword)
}

module.exports = register