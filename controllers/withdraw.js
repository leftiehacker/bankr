const makeTransaction = require('./util/makeTransaction')

const Type = 'Withdrawal'

function withdraw (Username, Amount) {
  makeTransaction(Type, Username, Amount)
}

module.exports = withdraw