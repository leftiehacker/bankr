const { users } = require('../models/db')
const errorHandler = require('./util/errorHandler')

function logout (Username) {
  users.findOne({ Username }, (err, user) => {
    errorHandler(err, () => {
      if (!user) throw 'LogoutError: Username not found.'
      if (!user.IsLoggedIn) throw 'LogoutError: User not logged in.'
    })
    
    users.update(
      { _id: user._id },
      { $set: { IsLoggedIn: false } },
      (err, r) => {
        errorHandler(err)
        console.log('\x1b[32m', `\n${Username} has logged out.`)
      }
    )
  })
}

module.exports = logout