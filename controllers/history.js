const { users, accounts, transactions } = require('../models/db')
const errorHandler = require('./util/errorHandler')

function history (Username) {
  const findAccount = (err, user) => {
    errorHandler(err, () => {
      if (!user) throw 'HistoryError: User does not exist.'
      if (!user.IsLoggedIn) throw 'HistoryError: Please log in.'
    })
    accounts.findOne({ UserID: user._id }, findTransactions)
  }

  const findTransactions = (err, account) => {
    errorHandler(err, () => {
      if (!account) throw 'HistoryError: Account does not exist.'
    })
    transactions.find({ AccountID: account._id }, logRecords)
  }

  const logRecords = (err, records) => {
    errorHandler(err)

    records.forEach(record => {
      const { Type, Amount, CreatedAt } = record
      console.log('\x1b[34m', `\n${CreatedAt.toLocaleDateString()} | ${Type} | ${Amount}`)
    })
  }

  users.findOne({ Username }, findAccount)
}

module.exports = history