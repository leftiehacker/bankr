const { users, accounts, transactions } = require('../../models/db')
const Transaction = require('../../models/Transaction')
const errorHandler = require('./errorHandler')

function makeTransaction (Type, Username, Amount) {  
  let userDoc;
  const findAccount = (err, user) => {
    errorHandler(err, () => {
      if (!user) throw 'TransactionError: Username not found.'
      if (!user.IsLoggedIn) throw 'TransactionError: Please log in.'
    })
    userDoc = user
    accounts.findOne({ UserID: user._id }, accountTransaction)
  }

  const accountTransaction = (err, account) => {
    errorHandler(err, () => {
      if (!account) throw 'TransactionError: No account found.'
      if (account.Balance < Amount && Type === 'Withdrawal') {
        throw 'TransactionError: Cannot withdraw more than the account balance.'
      }
    })

    let amount;
    let message;
    if (Type === 'Deposit') {
      amount = +Amount
      message = 'deposited to'
    } else if (Type === 'Withdrawal') {
      amount = -1 * +Amount
      message = 'withdrawn from'
    }

    accounts.update(
      { UserID: userDoc._id },
      { $inc: { Balance: amount } }
    )

    const transaction = new Transaction(account._id, Type, Amount)

    transactions.insert(transaction, (err, doc) => {
      errorHandler(err)
      console.log('\x1b[34m', `\n${Amount} was ${message} ${Username}'s account.`)
    })
  }

  // check if amount entered is a valid integer or float
  errorHandler(() => {
    if (!/^[1-9]\d*(\.\d+)?$/.test(Amount)) {
      throw 'TransactionError: Amount must be a positive number.'
    }
  })
  
  users.findOne({ Username }, findAccount)
}

module.exports = makeTransaction