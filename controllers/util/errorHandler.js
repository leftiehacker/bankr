function errorHandler(err=null, errFunc=null) {
  try {
    if (err && typeof err !== 'function') throw err
    if (typeof err === 'function') err()
    if (errFunc) errFunc()
  } catch (e) {
    console.error("\x1b[31m\n", e)
    process.exit(1);
  }
}

module.exports = errorHandler