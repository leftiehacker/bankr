const makeTransaction = require('./util/makeTransaction')

const Type = 'Deposit'

function deposit (Username, Amount) {
  makeTransaction(Type, Username, Amount)
}

module.exports = deposit