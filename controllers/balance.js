const { users, accounts } = require('../models/db')
const errorHandler = require('./util/errorHandler')

function balance (Username) {
  const findAccount = (err, user) => {
    errorHandler(err, () => {
      if (!user) throw 'BalanceError: Username not found.'
      if (!user.IsLoggedIn) throw 'BalanceError: Please log in.'
    })
    accounts.findOne({ UserID: user._id }, logBalance)
  }

  const logBalance = (err, account) => {
    errorHandler(err, () => {
      if (!account) throw 'BalanceError: No account found.'
    })
    console.log('\x1b[34m', `\nAccount Balance: ${account.Balance}`)
  }

  users.findOne({ Username }, findAccount)
}

module.exports = balance